﻿using Pokemons.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;

namespace Pokemons.Controllers
{
    public class HomeController : Controller
    {
        PokemonsContext db = new PokemonsContext();

        public ActionResult Index()
        {
            return View(db.Pokemons);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult AddNewPokemon()
        {
            return View();
        }

        [HttpPost]
        public string AddNewPokemon(Pokemon newPokemon)
        {
            db.Pokemons.Add(newPokemon);
            db.SaveChanges();
            return "Удалось...<a href='/Home/Index'>Главная</a></html>";
        }

        [HttpGet]
        public ActionResult EditPokemon(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Pokemon pokemon = db.Pokemons.Find(id);

            if (pokemon == null)
            {
                return HttpNotFound();
            }

            return View(pokemon);
        }

        [HttpPost]
        public ActionResult EditPokemon(Pokemon pokemon)
        {
            db.Entry(pokemon).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ShowDetails(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Pokemon pokemon = db.Pokemons.Find(id);

            if (pokemon == null)
            {
                return HttpNotFound();
            }

            return View(pokemon);
        }
    }
}