﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pokemons.Models
{
    public class Pokemon
    {
        public int PokemonId { get; set; }
        public string Name { get; set; }
        public int Helth { get; set; }
        public int Armor { get; set; }
        public int Damage { get; set; }
        public double ArmorIgnore { get; set; }
        public string Skill { get; set; }
    }
}