﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Pokemons.Models
{
    public class PokemonsContext : DbContext
    {
        public DbSet<Pokemon> Pokemons { get; set; }
    }
}